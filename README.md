# NGrama


# Problem Statement
> When processing voice input from a user, sometimes we won't be able to get the complete intended input our user made, and some guesswork will have to be made in order to accommodate for this problem.

# About the project
An n-gram is a contiguous sequence of n items from a given sample of text or speech. 

Creating a n-gram model lets us make predictions on what the next words on a sentence might be.

# Getting started

This is an Open-Source project that will help to predict or recommend words in phrases using and algorithm trained by a corpus given to a NGram

# Prerequisites
*   Python 3.x
*   nltk
*   nose
*   numpy
*   scipy
*   wsgiref
*   marisa-trie

# Installing
### Windows
#### https://www.ics.uci.edu/~pattis/common/handouts/pythoneclipsejava/python.html

### Linux
#### ``sudo apt-get install python3.6``

# Example using Ngram

# Testing
## Test the smoothing with a given file
``python Ngram.py smoothing file spanishText_15000_20000  ``

## Output
[Screenshot](https://imgur.com/jX0r7CM)
# Code
``python Ngram.py predict ``

``python Ngram.py smoothing file <file> ``

``python Ngram.py smoothing text "Text to learn from" ``
# Tools
* Visual Studio Code


### Integrantes
*  Marcos Almonte
*  Kevin Madrid
*  Daniel Madrid
*  Humberto Villanueva

# License

                    GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 